<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Theme 9 showcase</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="css/style.css"/>
  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>
<body>
  <div class="container-fluid mainContainer">
    <div class="row header">
      <h1>Showcase</h1>
      <h2>Theme 9 - Fairtrade</h2>
      <p class="team">Martin Arusalu, Balázs Gáspár, Ladislav Jankovič, Marinela Anca Giurca, Gilad Ben Yosef</p>
    </div>
    <div class="row communication faded">
      <div class="container padding">
        <div class="row">
          <div class="col-lg-6 col-md-6 padding">
            <img src="assets/img1.jpg">
          </div>
          <div class="col-lg-6 col-md-6 padding">
            <h2>Why buy Fairtrade?</h2>
            <ul>
              <li>Fairtrades products are mostly organic which makes them healthier and enviromental friendly.</li>
              <li>Buying Fairtrade not only helps the enviroment it helps your body.</li>
              <li>Fairtrade improves the life of the product producers as well as the workers. Meaning, they will receive a decent salary, housing and better living conditions in general.</li>
            </ul>
          </div>
        </div>
       </div>
       <div class="container padding">
        <div class="row">
          <div class="col-lg-6 col-md-6 padding">
            <h2>Cocoa industry</h2>
            <ul>
              <li>16% precent of cocoa farmers are children between the age of 5 and 17.</li>
              <li>The avarege salary of a cocoa farmer is 13 DKK per day.</li>
              <li>Despite their role in contributing to child labor, slavery, and human trafficking, the chocolate industry has not taken significant steps to remedy the problem.</li>
            </ul>
          </div>
          <div class="col-lg-6 col-md-6 padding">
            <img src="assets/img7.jpg">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class=" col-sm-offset-1 col-sm-10">
        <?php include "svg2.php" ?>
      </div>
    </div>
    <div class="row faded">
      <div class="col-sm-offset-2 col-sm-8">
        <h2>Motion Graphics</h2>
        <video controls>
        <source src="assets/motionGraphics.mp4" type="video/mp4">
          Your browser does not support HTML5 video.
        </video>
      </div>
    </div>
    <div class="row faded">
      <div class="col-sm-offset-2 col-sm-8">
        <h2>360 video</h2>
        <video controls>
        <source src="assets/360.mp4" type="video/mp4">
          Your browser does not support HTML5 video.
        </video>
      </div>
    </div>
  </div>
  <script src="js/snap.svg-min.js"></script>
  <script src="js/script.js"></script>
</body>
</html>