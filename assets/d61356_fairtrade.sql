-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2017 at 09:33 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `d61356_fairtrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `country` varchar(128) NOT NULL,
  `continent` varchar(45) NOT NULL,
  `population` int(11) DEFAULT NULL,
  `comments` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `country`, `continent`, `population`, `comments`) VALUES
(1, 'Ivory Coast', 'Africa', 22157107, NULL),
(2, 'Ghana', 'Africa', 26786598, NULL),
(3, 'Indonesia', 'Asia', 254454778, NULL),
(4, 'Nigeria', 'Africa', 177475986, NULL),
(5, 'Cameroon', 'Africa', 22773014, NULL),
(6, 'Brazil', 'South America', 206077898, NULL),
(7, 'Ecuador', 'South America', 15902916, NULL),
(8, 'Mexico', 'North America', 125385833, NULL),
(9, 'Peru', 'South America', 30973148, NULL),
(10, 'Dominican Republic', 'North America', 10405943, NULL),
(11, 'Columbia', 'South America', 47791393, NULL),
(12, 'Papua New Guinea', 'Australia / Oceania', 7463577, NULL),
(13, 'Venezuela', 'South America', 30693827, NULL),
(14, 'Uganda', 'Africa', 37782971, NULL),
(15, 'Togo', 'Africa', 7115163, NULL),
(16, 'Sierra Leone', 'Africa', 6315627, NULL),
(17, 'Guatemala', 'North America', 16015494, NULL),
(18, 'India', 'Asia', 1295291543, NULL),
(19, 'Haiti', 'North America', 10572029, NULL),
(20, 'Madagascar', 'Africa', 23571713, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `time` year(4) DEFAULT NULL,
  `price` decimal(10,0) NOT NULL,
  `comment` text,
  `product_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `id` int(11) NOT NULL,
  `amount` decimal(10,0) DEFAULT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `area_id` int(11) NOT NULL,
  `product_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `production`
--

INSERT INTO `production` (`id`, `amount`, `comment`, `area_id`, `product_type_id`) VALUES
(1, '1448992', NULL, 1, 1),
(2, '835466', NULL, 2, 1),
(3, '777500', NULL, 3, 1),
(4, '367000', NULL, 4, 1),
(5, '275000', NULL, 5, 1),
(6, '256186', NULL, 6, 1),
(7, '128446', NULL, 7, 1),
(8, '82000', NULL, 8, 1),
(9, '71175', NULL, 9, 1),
(10, '68021', NULL, 10, 1),
(11, '46739', NULL, 11, 1),
(12, '41200', NULL, 12, 1),
(13, '31236', NULL, 13, 1),
(14, '20000', NULL, 14, 1),
(15, '15000', NULL, 15, 1),
(16, '14850', NULL, 16, 1),
(17, '13127', NULL, 17, 1),
(18, '13000', NULL, 18, 1),
(19, '10000', NULL, 19, 1),
(20, '9000', NULL, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `description`, `comment`) VALUES
(1, 'Cocoa', 'Cocoa beans', NULL),
(2, 'Coffee', 'Coffee beans', NULL),
(3, 'Banana', 'Yellow bananas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `worker`
--

CREATE TABLE `worker` (
  `id` int(11) NOT NULL,
  `average_income` decimal(10,2) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `comment` text,
  `area_id` int(11) NOT NULL,
  `worker_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `worker`
--

INSERT INTO `worker` (`id`, `average_income`, `amount`, `comment`, `area_id`, `worker_type_id`) VALUES
(1, '0.50', 14, NULL, 1, 1),
(2, '1.25', 86, NULL, 1, 2),
(3, '0.00', 7, NULL, 1, 3),
(4, '0.84', 12, NULL, 2, 1),
(5, '0.00', 4, NULL, 2, 3),
(6, '1.10', 84, NULL, 2, 2),
(7, '0.74', 8, NULL, 4, 1),
(8, '1.28', 80, NULL, 4, 2),
(9, '0.00', 2, NULL, 4, 3),
(10, '0.68', 10, NULL, 5, 1),
(11, '1.00', 88, NULL, 5, 2),
(12, '0.00', 2, NULL, 5, 3),
(13, '1.30', 90, NULL, 14, 2),
(14, '0.90', 6, NULL, 14, 1),
(15, '0.00', 4, NULL, 14, 3),
(16, '1.18', 76, NULL, 15, 2),
(17, '1.00', 12, NULL, 15, 1),
(18, '0.00', 12, NULL, 15, 3),
(19, '1.32', 81, NULL, 16, 2),
(20, '0.87', 10, NULL, 16, 1),
(21, '0.00', 9, NULL, 16, 3),
(22, '1.40', 91, NULL, 20, 2),
(23, '0.98', 7, NULL, 20, 1),
(24, '0.00', 2, NULL, 20, 3);

-- --------------------------------------------------------

--
-- Table structure for table `worker_type`
--

CREATE TABLE `worker_type` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `worker_type`
--

INSERT INTO `worker_type` (`id`, `name`, `description`, `comment`) VALUES
(1, 'Child', 'Children in the age of 5 - 17', 'Workers'),
(2, 'Adult', 'Workers in the age of above 17', 'Workers'),
(3, 'Forced', 'Forced child labor', 'Children, who are either kidnapped or on some other way forced to work in the cocoa fields.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`,`product_type_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_price_product_type1_idx` (`product_type_id`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`id`,`area_id`,`product_type_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_product_area_idx` (`area_id`),
  ADD KEY `fk_product_product_type1_idx` (`product_type_id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`,`area_id`,`worker_type_id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD KEY `fk_worker_area1_idx` (`area_id`),
  ADD KEY `fk_worker_worker_type1_idx` (`worker_type_id`);

--
-- Indexes for table `worker_type`
--
ALTER TABLE `worker_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `worker_type`
--
ALTER TABLE `worker_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `price`
--
ALTER TABLE `price`
  ADD CONSTRAINT `fk_price_product_type1` FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `production`
--
ALTER TABLE `production`
  ADD CONSTRAINT `fk_product_area` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_product_type1` FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `worker`
--
ALTER TABLE `worker`
  ADD CONSTRAINT `fk_worker_area1` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_worker_worker_type1` FOREIGN KEY (`worker_type_id`) REFERENCES `worker_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
